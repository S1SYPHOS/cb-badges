from os.path import abspath, basename, dirname, isdir, isfile, join

from diskcache import Cache
from pytest import raises

from cb_badges import Badges


# Setup
# (1) Fixtures
fixtures = join(abspath(dirname(__file__)), 'fixtures')

# (2) Valid template directories ..
valid_templates = [
    # (a) .. by shorthand
    'codeberg',
    'standard',

    # (b) .. by path
    join(fixtures, 'templates', 'valid'),
]


# (3) Invalid template directories ..
invalid_templates = [
    # (a) .. by shorthand
    '',
    'no_template',

    # (b) .. by path
    join(fixtures, 'templates', 'no_config'),
    join(fixtures, 'templates', 'no_template'),
]

# (4) Valid themes ..
valid_themes = [
    # (a) .. by shorthand
    'blue',
    'gray',
    'teal',

    # (b) .. by path
    join(fixtures, 'themes', 'simple.json'),
]

# (5) Invalid themes ..
invalid_themes = [
    # (a) .. by shorthand
    '',
    'no-theme',

    # (b) .. by path
    join(fixtures, 'themes', 'invalid.json'),
]

# (6) Valid fonts ..
valid_fonts = [
    # (a) .. by shorthand
    'inter-regular',
    'hack-italic',

    # (b) .. by path
    join(fixtures, 'fonts', 'Gidole-Regular.ttf'),
]

# (7) Invalid fonts ..
invalid_fonts = [
    # (a) .. by shorthand
    'inter',
    'hack',

    # (b) .. by path
    join(fixtures, 'fonts', 'Invalid.otf'),
]


def test_init():
    # Run function
    obj = Badges()

    # Assert results
    # (1) Template
    assert obj.template == 'standard'

    # (2) Cache
    assert isinstance(obj.cache, Cache)


def test_get_template_dir():
    # Setup
    # (1) Object
    obj = Badges()

    # Run function #1
    for directory in valid_templates:
        # Assert result
        assert isdir(obj.get_template_dir(directory))

    # Run function #2
    for directory in invalid_templates:
        # Assert exception
        with raises(ValueError):
            obj.get_template_dir(directory)


def test_get_theme():
    # Setup
    # (1) Object
    obj = Badges()

    # Run function #1
    for theme in valid_themes:
        # Assert result
        assert isfile(obj.get_theme(theme))

    # Run function #2
    for theme in invalid_themes:
        # Assert exception
        with raises(ValueError):
            obj.get_theme(theme)


def test_get_font():
    # Setup
    # (1) Object
    obj = Badges()

    # Run function #1
    for font in valid_fonts:
        # Assert result
        assert isfile(obj.get_font(font))

    # Run function #2
    for font in invalid_fonts:
        # Assert exception
        with raises(ValueError):
            obj.get_font(font)


def test_set_template():
    # Setup
    # (1) Object
    obj = Badges()

    # Assert default
    assert obj.template == 'standard'

    # Run function #1
    for template in valid_templates:
        # Assert result #1
        obj.template = template
        assert obj.template in template

        # Assert result #2
        badges = Badges(template)
        assert badges.template in template

    # Run function #2
    for template in invalid_templates:
        # Assert exception #1
        with raises(ValueError):
            obj.template = template

        # Assert exception #2
        with raises(ValueError):
            Badges(template)


def test_render_text():
    # Setup
    # (1) Object
    obj = Badges()

    # (2) Text
    text = 'test'

    # (3) Font name
    font = 'hack-regular'

    # (4) Font size
    font_size = 32

    # Run function
    result = obj.render_text(text, font, font_size)

    # Assert result
    assert result == {
        'path': '<path d="M 12.609375 0.0625 C 10.460938 0.0625 8.941406 -0.367188 8.046875 -1.234375 C 7.160156 -2.109375 6.71875 -3.597656 6.71875 -5.703125 L 6.71875 -15.265625 L 2.046875 -15.265625 L 2.046875 -17.5 L 6.71875 -17.5 L 6.71875 -22.078125 L 9.59375 -23.328125 L 9.59375 -17.5 L 16.125 -17.5 L 16.125 -15.265625 L 9.59375 -15.265625 L 9.59375 -5.703125 C 9.59375 -4.421875 9.835938 -3.519531 10.328125 -3 C 10.816406 -2.488281 11.671875 -2.234375 12.890625 -2.234375 L 16.125 -2.234375 L 16.125 0.0625 Z M 29.859375 0.453125 C 27.160156 0.453125 25.039062 -0.351562 23.5 -1.96875 C 21.96875 -3.59375 21.203125 -5.804688 21.203125 -8.609375 C 21.203125 -10.347656 21.503906 -11.921875 22.109375 -13.328125 C 22.722656 -14.734375 23.628906 -15.847656 24.828125 -16.671875 C 26.035156 -17.503906 27.523438 -17.921875 29.296875 -17.921875 C 31.628906 -17.921875 33.4375 -17.171875 34.71875 -15.671875 C 36.007812 -14.179688 36.65625 -12.113281 36.65625 -9.46875 L 36.65625 -8.0625 L 24.203125 -8.0625 L 24.203125 -7.96875 C 24.203125 -6.144531 24.679688 -4.691406 25.640625 -3.609375 C 26.597656 -2.523438 28.015625 -1.984375 29.890625 -1.984375 C 30.972656 -1.984375 32.019531 -2.15625 33.03125 -2.5 C 34.050781 -2.84375 35.035156 -3.257812 35.984375 -3.75 L 35.984375 -0.890625 C 34.992188 -0.484375 33.988281 -0.160156 32.96875 0.078125 C 31.957031 0.328125 30.921875 0.453125 29.859375 0.453125 Z M 33.78125 -10.3125 C 33.78125 -11.28125 33.632812 -12.15625 33.34375 -12.9375 C 33.050781 -13.71875 32.582031 -14.335938 31.9375 -14.796875 C 31.300781 -15.253906 30.457031 -15.484375 29.40625 -15.484375 C 28.34375 -15.484375 27.453125 -15.25 26.734375 -14.78125 C 26.023438 -14.320312 25.46875 -13.703125 25.0625 -12.921875 C 24.664062 -12.140625 24.414062 -11.269531 24.3125 -10.3125 Z M 47.734375 0.453125 C 46.066406 0.453125 44.109375 0.0859375 41.859375 -0.640625 L 41.859375 -3.609375 C 44.046875 -2.503906 45.992188 -1.953125 47.703125 -1.953125 C 48.921875 -1.953125 49.894531 -2.207031 50.625 -2.71875 C 51.351562 -3.226562 51.71875 -3.910156 51.71875 -4.765625 C 51.71875 -5.503906 51.410156 -6.097656 50.796875 -6.546875 C 50.191406 -7.003906 49.222656 -7.375 47.890625 -7.65625 L 46.6875 -7.90625 C 45.039062 -8.238281 43.832031 -8.785156 43.0625 -9.546875 C 42.300781 -10.316406 41.921875 -11.363281 41.921875 -12.6875 C 41.921875 -14.363281 42.492188 -15.65625 43.640625 -16.5625 C 44.785156 -17.46875 46.414062 -17.921875 48.53125 -17.921875 C 50.320312 -17.921875 52.054688 -17.578125 53.734375 -16.890625 L 53.734375 -14.078125 C 52.097656 -15.035156 50.390625 -15.515625 48.609375 -15.515625 C 46.085938 -15.515625 44.828125 -14.65625 44.828125 -12.9375 C 44.828125 -12.375 44.9375 -11.925781 45.15625 -11.59375 C 45.375 -11.269531 45.789062 -10.992188 46.40625 -10.765625 C 47.019531 -10.546875 47.925781 -10.316406 49.125 -10.078125 L 50.28125 -9.859375 C 53.175781 -9.285156 54.625 -7.707031 54.625 -5.125 C 54.625 -3.394531 54.007812 -2.03125 52.78125 -1.03125 C 51.550781 -0.0390625 49.867188 0.453125 47.734375 0.453125 Z M 70.40625 0.0625 C 68.257812 0.0625 66.738281 -0.367188 65.84375 -1.234375 C 64.957031 -2.109375 64.515625 -3.597656 64.515625 -5.703125 L 64.515625 -15.265625 L 59.84375 -15.265625 L 59.84375 -17.5 L 64.515625 -17.5 L 64.515625 -22.078125 L 67.390625 -23.328125 L 67.390625 -17.5 L 73.921875 -17.5 L 73.921875 -15.265625 L 67.390625 -15.265625 L 67.390625 -5.703125 C 67.390625 -4.421875 67.632812 -3.519531 68.125 -3 C 68.613281 -2.488281 69.46875 -2.234375 70.6875 -2.234375 L 73.921875 -2.234375 L 73.921875 0.0625 Z M 77.0625 0 " />',
        'width': 71.875,
        'height': 23.78125,
    }


def test_font_metrics():
    # Setup
    # (1) Object
    obj = Badges()

    # (2) Font name
    font = 'hack-regular'

    # (3) Font size
    font_size = 32

    # Run function
    result = obj.font_metrics(font, font_size)

    # Assert result
    # (1) Metrics object
    assert 'metrics' in result

    # (2) Computed values
    assert result['height'] == 37.25
    assert result['x_height'] == 17.5
    assert result['ascent'] == 29.703125
    assert result['descent'] == 7.546875


def test_render():
    # Setup
    # (1) Object
    obj = Badges()

    # (2) Vanilla
    with open(join(fixtures, 'svg', 'vanilla.svg'), 'r') as file:
        vanilla = file.read()

    # (2) Custom
    with open(join(fixtures, 'svg', 'custom.svg'), 'r') as file:
        custom = file.read()

    data = {
        'height': 30,
        'font_size': 20,
        'roundness': 0,
        'x_spacing': 10.5,
        'has_shadow': True,
        'swap_colors': True,
        'font_left': 'hack-italic',
        'font_right': 'inter-semibold',
        'text_left': 'test 1',
        'text_right': 'test 2',
        'bg_left': '#ccc',
        'bg_right': '#ddd',
        'color_left': '#000',
        'color_right': '#111',
        'shadow_left': '#222',
        'shadow_right': '#333',
    }

    # Run function #1
    result1 = obj.render({})

    # Assert result
    assert result1 == vanilla

    # Run function #1
    result2 = obj.render(data)

    # Assert result
    assert result2 == custom
