from click.testing import CliRunner

from cb_badges.cli import cli


def test_no_argument():
    # Setup
    # (1) CLI runner
    runner = CliRunner()

    # Run function
    result = runner.invoke(cli)

    # Assert result
    # (1) Exit code
    assert result.exit_code == 2

    # (2) Output text
    assert 'Missing argument' in result.output
